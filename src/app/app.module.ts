import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ReactiveFormsModule } from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { InscriptionComponent } from './pages/inscription/inscription.component';
import { FormulaireComponent } from './components/formulaire/formulaire.component';
import { UtilisateurService } from './services/utilisateur.service';
import { ProfessionnelService } from './services/professionnel.service';
import { EvenementService } from './services/evenement.service';
import { FormulaireProComponent } from './components/formulaire-pro/formulaire-pro.component';
import { MenuComponent } from './components/menu/menu.component';
import { ConnexionComponent } from './pages/connexion/connexion.component';
import { FormulaireConnexionComponent } from './components/formulaire-connexion/formulaire-connexion.component';
import { ConnexionService } from './services/connexion.service';
import { FormulaireConnexionProComponent } from './components/formulaire-connexion-pro/formulaire-connexion-pro.component';
import { CreationEvenementComponent } from './pages/creation-evenement/creation-evenement.component';
import { RechercheEvenementComponent } from './pages/recherche-evenement/recherche-evenement.component';
import { AffichageEvenementComponent } from './pages/affichage-evenement/affichage-evenement.component';
import { AffichageEvenementProComponent } from './pages/affichage-evenement-pro/affichage-evenement-pro.component';
import { ListeComponent } from './components/liste/liste.component';
import { RechercheComponent } from './components/recherche/recherche.component';
import { CreationComponent } from './components/creation/creation.component';
import { ListeProComponent } from './components/liste-pro/liste-pro.component';
import { DetailComponent } from './pages/detail/detail.component';
import { DetailProComponent } from './pages/detail-pro/detail-pro.component';
import { AffichageProComponent } from './pages/affichage-pro/affichage-pro.component';
import { FavorisComponent } from './pages/favoris/favoris.component';
import {MatButtonModule} from '@angular/material/button';
import { ProfilComponent } from './pages/profil/profil.component';
import { CompteComponent } from './components/compte/compte.component';
import { CompteProComponent } from './components/compte-pro/compte-pro.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ModificationComponent } from './pages/modification/modification.component';






@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    InscriptionComponent,
    FormulaireComponent,
    FormulaireProComponent,
    MenuComponent,
    ConnexionComponent,
    FormulaireConnexionComponent,
    FormulaireConnexionProComponent,
    CreationEvenementComponent,
    RechercheEvenementComponent,
    AffichageEvenementComponent,
    AffichageEvenementProComponent,
    ListeComponent,
    RechercheComponent,
    CreationComponent,
    ListeProComponent,    
    DetailComponent, 
    DetailProComponent, 
    AffichageProComponent,
    FavorisComponent,
    ProfilComponent,
    CompteComponent,
    CompteProComponent,
    ModificationComponent
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatInputModule,
    MatToolbarModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatListModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatSnackBarModule,
    AngularEditorModule
    
    

    
  ],
  providers: [UtilisateurService,ProfessionnelService,EvenementService,ConnexionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
