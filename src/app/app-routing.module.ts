import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { InscriptionComponent } from './pages/inscription/inscription.component';
import { ConnexionComponent } from './pages/connexion/connexion.component';
import { AffichageEvenementComponent } from './pages/affichage-evenement/affichage-evenement.component';
import { AffichageEvenementProComponent } from './pages/affichage-evenement-pro/affichage-evenement-pro.component';
import { CreationEvenementComponent } from './pages/creation-evenement/creation-evenement.component';
import { RechercheEvenementComponent } from './pages/recherche-evenement/recherche-evenement.component';
import { DetailComponent } from './pages/detail/detail.component';
import { DetailProComponent } from './pages/detail-pro/detail-pro.component';
import { AffichageProComponent } from './pages/affichage-pro/affichage-pro.component';
import { FavorisComponent } from './pages/favoris/favoris.component';
import { ProfilComponent } from './pages/profil/profil.component';
import { ModificationComponent } from './pages/modification/modification.component';


const routes: Routes = [{path: 'accueil', component: AccueilComponent},
                        {path: 'inscription', component: InscriptionComponent},
                        {path: 'connexion', component: ConnexionComponent},
                        {path: 'affichage', component: AffichageEvenementComponent},
                        {path: 'affichagePro', component: AffichageEvenementProComponent},
                        {path: 'creation', component: CreationEvenementComponent},
                        {path: 'recherche', component: RechercheEvenementComponent},
                        {path: 'detail/:id', component: DetailComponent},
                        {path: 'detailPro/:id', component: DetailProComponent},
                        {path: 'professionnel/:id', component: AffichageProComponent},
                        {path: 'favoris', component: FavorisComponent},
                        {path: 'profil', component: ProfilComponent},
                        {path: 'modification/:id', component: ModificationComponent}
                        
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
