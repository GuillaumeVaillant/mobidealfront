import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { Utilisateur } from 'src/app/models/utilisateur';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.scss']
})
export class CompteComponent implements OnInit {
  utilisateur:Utilisateur;
  utilisateursForm: FormGroup;

  constructor(private utilisateurService:UtilisateurService,private fb:FormBuilder, private router:Router,private popup: MatSnackBar) {
    this.utilisateursForm = fb.group({
      nom:"",
      prenom:"",
      adresse:"",
      telephone:"",
      mail:"",
      password:""
    })
   }

  ngOnInit() {
    this.utilisateurService.getById(localStorage.getItem("id")).subscribe(
      (data:Utilisateur) => {this.utilisateur = data;},
      err => {console.log(err);}
    );
    
  }
  modifier = () => {
    if(this.utilisateursForm.value.nom !==""){
      let utilisateur = new Utilisateur();
      utilisateur.id = this.utilisateur.id
      utilisateur.nom = this.utilisateursForm.value.nom;
      utilisateur.prenom = this.utilisateursForm.value.prenom;
      utilisateur.adresse = this.utilisateursForm.value.adresse;
      utilisateur.telephone = this.utilisateursForm.value.telephone;
      utilisateur.mail = this.utilisateursForm.value.mail;
      utilisateur.password = this.utilisateursForm.value.password;
      utilisateur.professionnels = this.utilisateur.professionnels;      
      this.utilisateurService.postUtilisateur(utilisateur).subscribe(
        () => {},
        err => console.warn(err)
      );
      this.popup.open("Modification faite","Fermer");     
    }   
  }
  supprimer = () => {
    this.utilisateurService.deleteById(localStorage.getItem("id")).subscribe(
      () => {},
        err => console.warn(err)
    );
    localStorage.removeItem("nom");
    localStorage.removeItem("id");
    localStorage.removeItem("adresse");    
    localStorage.removeItem("status");
    this.router.navigate(['/accueil'])
  }

}


