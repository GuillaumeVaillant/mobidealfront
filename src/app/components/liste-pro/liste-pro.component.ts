import { Component, OnInit } from '@angular/core';
import { Evenement } from 'src/app/models/evenement';
import { EvenementService } from 'src/app/services/evenement.service';

@Component({
  selector: 'app-liste-pro',
  templateUrl: './liste-pro.component.html',
  styleUrls: ['./liste-pro.component.scss']
})
export class ListeProComponent implements OnInit {
  data:Evenement[];
  displayedColumns: string[] = ['nom','tag','dateDebut', 'dateFin'];
  nom:string;
  
  constructor(private evenementService:EvenementService) { }

  ngOnInit() {
    this.nom = localStorage.getItem("nom");
    this.evenementService.getAllEvenementByProfessionnel(localStorage.getItem("nom")).subscribe(
      (data:Evenement[])=>{ this.data = data;},
        err => {console.warn(err)}
    );
    
    

  }

}
