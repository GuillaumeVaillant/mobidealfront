import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EvenementService } from 'src/app/services/evenement.service';
import { Router } from '@angular/router';
import { Evenement } from 'src/app/models/evenement';
import { ProfessionnelService } from 'src/app/services/professionnel.service';
import { Professionnel } from 'src/app/models/professionnel';
import { AngularEditorConfig } from '@kolkov/angular-editor';


@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.scss']
})
export class CreationComponent implements OnInit {
  creationForm:FormGroup;
  data:Professionnel;
  data2:Evenement;

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '3000',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
     
      ['fontSize']
    ]
};
  
  constructor(private fb:FormBuilder,private evenementService:EvenementService,private professionnelService:ProfessionnelService,private router:Router) {
    this.creationForm = fb.group({
      nom:"",
      tag:"",
      lieu:"",
      dateDebut:"",
      dateFin:"",
      contenu:""

    })
   }

  ngOnInit() {
  }
  creation = () => {
    if(this.creationForm.value.nom !==""){
      let evenement = new Evenement();
      evenement.nom = this.creationForm.value.nom;
      evenement.tag = this.creationForm.value.tag;
      evenement.lieu = this.creationForm.value.lieu;
      evenement.dateDebut = this.creationForm.value.dateDebut;
      evenement.dateFin = this.creationForm.value.dateFin;
      evenement.contenu = this.creationForm.value.contenu;
      evenement.professionnel.id = localStorage.getItem("id")
      this.evenementService.postEvenement(evenement).subscribe(
        () => {},
        err => {console.warn(err)}
      );
      this.router.navigate(["/affichagePro"]);
    }else{
      this.router.navigate(["/creation"]);
    }
        
    
  }
}
