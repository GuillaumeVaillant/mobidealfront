import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder} from '@angular/forms';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { Utilisateur } from 'src/app/models/utilisateur';
import { Router } from '@angular/router';



@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent implements OnInit {
  utilisateursForm: FormGroup;

  constructor(private fb:FormBuilder, private utilisateurService:UtilisateurService, private router:Router) { 

    this.utilisateursForm = fb.group({
      nom:"",
      prenom:"",
      adresse:"",
      telephone:"",
      mail:"",
      password:""
    })
  }

  ngOnInit() {
  }

  envoyer = () =>{
    if(this.utilisateursForm.value.nom !==""){
      let utilisateur = new Utilisateur();
      utilisateur.nom = this.utilisateursForm.value.nom;
      utilisateur.prenom = this.utilisateursForm.value.prenom;
      utilisateur.adresse = this.utilisateursForm.value.adresse;
      utilisateur.telephone = this.utilisateursForm.value.telephone;
      utilisateur.mail = this.utilisateursForm.value.mail;
      utilisateur.password = this.utilisateursForm.value.password;      
      this.utilisateurService.postUtilisateur(utilisateur).subscribe(
        () => {},
        err => console.warn(err)
      );
      this.router.navigate(["/connexion"])
    }else{
      this.router.navigate(["/inscription"])
    }


  }

  
  


}
