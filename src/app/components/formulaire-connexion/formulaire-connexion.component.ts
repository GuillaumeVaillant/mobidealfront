import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { RechercheUtilisateur } from 'src/app/models/recherche-utilisateur';
import { Utilisateur } from 'src/app/models/utilisateur';
import { Router } from '@angular/router';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-formulaire-connexion',
  templateUrl: './formulaire-connexion.component.html',
  styleUrls: ['./formulaire-connexion.component.scss']
})
export class FormulaireConnexionComponent implements OnInit {
  connecteForm: FormGroup;
  data:Utilisateur;
  
  
  constructor(private fb:FormBuilder,private utilisateurService:UtilisateurService, private router:Router, private connexionService:ConnexionService) { 
    this.connecteForm = fb.group({
      mail:"",
      password:""
    })
  }

  ngOnInit() {
  }

  connexion = () => {
    let rechercheUtilisateur = new RechercheUtilisateur();
    rechercheUtilisateur.mail = this.connecteForm.value.mail;
    rechercheUtilisateur.password = this.connecteForm.value.password;     
    this.utilisateurService.connexionUtilisateur(rechercheUtilisateur).subscribe(
      (data:Utilisateur) => {this.data = data;},
        err => {console.warn(err)}
    )
    
    setTimeout( () =>{
      localStorage.setItem("nom",this.data.nom);
      localStorage.setItem("id",this.data.id);
      localStorage.setItem("status","particulier");

      if(localStorage.length === 0){                 
        this.router.navigate(['/connexion']);
      }else{                  
        this.router.navigate(['/recherche']);      
      }
      
    },100);
      
    

  }

}
