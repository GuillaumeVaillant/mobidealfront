import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireProComponent } from './formulaire-pro.component';

describe('FormulaireProComponent', () => {
  let component: FormulaireProComponent;
  let fixture: ComponentFixture<FormulaireProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
