import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProfessionnelService } from 'src/app/services/professionnel.service';
import { Professionnel } from 'src/app/models/professionnel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulaire-pro',
  templateUrl: './formulaire-pro.component.html',
  styleUrls: ['./formulaire-pro.component.scss']
})
export class FormulaireProComponent implements OnInit {
  professionnelsForm:FormGroup;
  lienSociaux:Array<string> = new Array();

  constructor(private fb:FormBuilder, private professionnelService:ProfessionnelService, private router:Router) { 
    this.professionnelsForm = fb.group({
      nom:"",
      mail:"",
      telephone:"",
      adresse:"",
      lienPersonnel:"",
      lienFacebook:"",
      lienTwitter:"",
      secteurActivite:"",
      password:""
    }) 
  }

  ngOnInit() {
  }
  envoyer = () =>{
    if(this.professionnelsForm.value.nom !== ""){
      let professionnel= new Professionnel();
      professionnel.nom = this.professionnelsForm.value.nom;
      professionnel.mail = this.professionnelsForm.value.mail;
      professionnel.telephone = this.professionnelsForm.value.telephone;
      professionnel.adresse = this.professionnelsForm.value.adresse;
      this.lienSociaux.push(this.professionnelsForm.value.lienPersonnel);
      this.lienSociaux.push(this.professionnelsForm.value.lienFacebook);
      this.lienSociaux.push(this.professionnelsForm.value.lienTwitter);
      professionnel.lienSociaux = this.lienSociaux;
      professionnel.secteurActivite = this.professionnelsForm.value.secteurActivite;
      professionnel.password = this.professionnelsForm.value.password;      
      this.professionnelService.postProfessionnel(professionnel).subscribe(
        () => {},
        err => console.warn(err)
      );
      this.router.navigate(["/connexion"])      
    }else{
      this.router.navigate(["/inscription"])
    }
    
  }


}
