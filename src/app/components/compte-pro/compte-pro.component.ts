import { Component, OnInit } from '@angular/core';
import { Professionnel } from 'src/app/models/professionnel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProfessionnelService } from 'src/app/services/professionnel.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-compte-pro',
  templateUrl: './compte-pro.component.html',
  styleUrls: ['./compte-pro.component.scss']
})
export class CompteProComponent implements OnInit {
  professionnel:Professionnel;
  professionnelsForm: FormGroup;
  lienSociaux:Array<string> = new Array();
  liens:string[];
  lien:string;
  facebook:string;
  twitter:string;

  constructor(private fb:FormBuilder, private professionnelService:ProfessionnelService, private router:Router,private popup: MatSnackBar) {
    this.professionnelsForm = fb.group({
      nom:"",
      mail:"",
      telephone:"",
      adresse:"",
      lienPersonnel:"",
      lienFacebook:"",
      lienTwitter:"",
      secteurActivite:"",
      password:""
    })
   }

  ngOnInit() {
    this.professionnelService.getById(localStorage.getItem("id"))
    .subscribe(
      (data:Professionnel) => {this.professionnel = data;},
      err => {console.log(err);}
    );
    setTimeout( () =>{
      this.liens = this.professionnel.lienSociaux.slice();
      this.lien = this.liens[0];
      console.log(this.lien);
      this.facebook = this.liens[1];
      this.twitter = this.liens[2];
    },100);


  }
  modifier = () => {    
      let professionnel= new Professionnel();
      professionnel.id = this.professionnel.id;
      professionnel.nom = this.professionnelsForm.value.nom;
      professionnel.mail = this.professionnelsForm.value.mail;
      professionnel.telephone = this.professionnelsForm.value.telephone;
      professionnel.adresse = this.professionnelsForm.value.adresse;
      this.lienSociaux.splice(0,1,this.professionnelsForm.value.lienPersonnel);
      this.lienSociaux.splice(1,1,this.professionnelsForm.value.lienFacebook);
      this.lienSociaux.splice(2,1,this.professionnelsForm.value.lienTwitter);
      professionnel.lienSociaux = this.lienSociaux;
      professionnel.secteurActivite = this.professionnelsForm.value.secteurActivite;
      professionnel.password = this.professionnelsForm.value.password;      
      this.professionnelService.postProfessionnel(professionnel).subscribe(
        () => {},
        err => console.warn(err)
      );
      this.popup.open("Modification faite","Fermer");
    
  }
  supprimer = () => {
    this.professionnelService.deleteById(localStorage.getItem("id")).subscribe(
      () => {},
        err => console.warn(err)
    );
    localStorage.removeItem("nom");
    localStorage.removeItem("id");
    localStorage.removeItem("adresse");    
    localStorage.removeItem("status");
    this.router.navigate(['/accueil'])
  }

}
