import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteProComponent } from './compte-pro.component';

describe('CompteProComponent', () => {
  let component: CompteProComponent;
  let fixture: ComponentFixture<CompteProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
