import { Component, OnInit } from '@angular/core';
import { EvenementService } from 'src/app/services/evenement.service';
import { Evenement } from 'src/app/models/evenement';
import { Router } from '@angular/router';


@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.scss']
})
export class ListeComponent implements OnInit {
  data:Evenement[];
  displayedColumns: string[] = ['nom', 'professionnel'];
  lieu:string;
  
  constructor(private evenementService:EvenementService, private router:Router) { }

  ngOnInit() {
    this.lieu = localStorage.getItem("lieu");
    this.evenementService.getAllEvenementByLieu(localStorage.getItem("lieu")).subscribe(
      (data:Evenement[])=>{ this.data = data;},
        err => {console.warn(err)}
    );
    
  }
  

}
