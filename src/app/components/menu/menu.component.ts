import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConnexionService } from 'src/app/services/connexion.service';



@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  connexion:boolean = true;
  particulier:boolean = false;
  professionnel:boolean =false;
  
  

  constructor(private router:Router,private connexionService:ConnexionService) {
     }

  ngOnInit() {
    this.affichage()
    
    
  }
  affichage= () =>{
    if(localStorage.length === 0){
      this.connexion=true;    
    }else{
      if(localStorage.getItem("status")==="particulier"){
        this.particulier=true;
        this.connexion=false;
      }else if(localStorage.getItem("status")==="professionnel"){
        this.professionnel=true;
        this.connexion=false;
      }else{
        this.connexion=true;
      }

    }
  }

  connexionParticulier = () => {
    this.connexionService.particulier=true;
    this.connexionService.professionnel=false;
    this.connexion=false;
    this.particulier=true;
    this.professionnel=false;
    localStorage.setItem("status","particulier")
    this.router.navigate(['/connexion'])
    
  }
  connexionProfessionnel = () => {
    this.connexionService.professionnel=true;
    this.connexionService.particulier=false;
    this.connexion=false;
    this.particulier=false;
    this.professionnel=true;
    localStorage.setItem("status","professionnel")
    this.router.navigate(['/connexion'])
    
  }

  deconnexion = () => {
    localStorage.removeItem("nom");
    localStorage.removeItem("id");
    localStorage.removeItem("adresse");
    localStorage.removeItem("lieu");
    localStorage.removeItem("status");
    this.particulier=false;    
    if(this.connexionService.particulier){
      this.connexionService.particulier=false;
    }
    if(this.connexionService.professionnel){
      this.connexionService.professionnel=false;
    }
    this.affichage();
    this.router.navigate(['/accueil'])
    
  }
  

}
