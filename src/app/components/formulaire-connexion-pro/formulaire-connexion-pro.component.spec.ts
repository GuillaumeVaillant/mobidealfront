import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireConnexionProComponent } from './formulaire-connexion-pro.component';

describe('FormulaireConnexionProComponent', () => {
  let component: FormulaireConnexionProComponent;
  let fixture: ComponentFixture<FormulaireConnexionProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireConnexionProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireConnexionProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
