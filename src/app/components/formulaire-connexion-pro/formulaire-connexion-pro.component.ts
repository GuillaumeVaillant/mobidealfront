import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Professionnel } from 'src/app/models/professionnel';
import { ProfessionnelService } from 'src/app/services/professionnel.service';
import { Router } from '@angular/router';
import { RechercheProfessionnel } from 'src/app/models/recherche-professionnel';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-formulaire-connexion-pro',
  templateUrl: './formulaire-connexion-pro.component.html',
  styleUrls: ['./formulaire-connexion-pro.component.scss']
})
export class FormulaireConnexionProComponent implements OnInit {
  connecteForm: FormGroup;
  data:Professionnel;

  constructor(private fb:FormBuilder,private professionnelService:ProfessionnelService, private router:Router, private connexionService:ConnexionService) {
    this.connecteForm = fb.group({
      mail:"",
      password:""
    })
   }

  ngOnInit() {}

  connexion = () =>{
    let rechercheProfessionnel = new RechercheProfessionnel();
    rechercheProfessionnel.mail = this.connecteForm.value.mail;
    rechercheProfessionnel.password = this.connecteForm.value.password;
    this.professionnelService.connexionProfessionnel(rechercheProfessionnel).subscribe(
      (data:Professionnel) => {this.data = data;},
      err => {console.warn(err)}
    )
    setTimeout( () =>{
      localStorage.setItem("id",this.data.id);
      localStorage.setItem("nom",this.data.nom);      
      localStorage.setItem("status","professionnel")
      if(localStorage.length === 0){                 
        this.router.navigate(['/connexion']);
      }else{      
        this.router.navigate(['/affichagePro']);
      }
    },100);
  }
  

}
