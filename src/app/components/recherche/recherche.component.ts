import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Evenement } from 'src/app/models/evenement';
import { EvenementService } from 'src/app/services/evenement.service';
import { RechercheEvenement } from 'src/app/models/recherche-evenement';
import { Router } from '@angular/router';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.scss']
})
export class RechercheComponent implements OnInit {
  affichage:boolean=false;
  rechercheForm:FormGroup;
  data:Evenement[];
  displayedColumns: string[] = ['nom', 'professionnel','tag' ];
  
  
  
  constructor(private fb:FormBuilder,private evenementService:EvenementService, private router:Router, private connexionService:ConnexionService) {
    
    this.rechercheForm = fb.group({
      tag:"",
      lieu:connexionService.recherche,
      date:"",
      

    })
   }

  ngOnInit() {
     
       
  }
  recherche = () =>{
    let rechercheEvenement = new RechercheEvenement();
    rechercheEvenement.tag = this.rechercheForm.value.tag;
    rechercheEvenement.lieu = this.rechercheForm.value.lieu;
    rechercheEvenement.date = this.rechercheForm.value.date;
    this.connexionService.recherche=this.rechercheForm.value.lieu;
    console.log(rechercheEvenement.tag);
    this.evenementService.postEvenementRecherche(rechercheEvenement).subscribe(
      (data:Evenement[]) => {this.data = data;},
        err => {console.warn(err)}
    )
    this.affichage=true;
    this.router.navigate(['/recherche'])

  }

}
