import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mobideal';

  constructor(private router:Router){
    
  }

  ngOnInit() {
    if(localStorage.length === 0){
      this.router.navigate(["/accueil"]);
    }

    

  }
}
