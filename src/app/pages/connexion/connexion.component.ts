import { Component, OnInit } from '@angular/core';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  particulier:boolean = false;
  professionnel:boolean = false;

  constructor(private connexionService:ConnexionService) { }

  ngOnInit() {
    if(localStorage.getItem("status")==="particulier"){
      this.particulier = true;
    }else if(localStorage.getItem("status")==="professionnel"){
      this.professionnel = true;
    }



  }

}
