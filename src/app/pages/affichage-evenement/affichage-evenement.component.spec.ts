import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageEvenementComponent } from './affichage-evenement.component';

describe('AffichageEvenementComponent', () => {
  let component: AffichageEvenementComponent;
  let fixture: ComponentFixture<AffichageEvenementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageEvenementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageEvenementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
