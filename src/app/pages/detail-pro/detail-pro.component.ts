import { Component, OnInit } from '@angular/core';
import { Evenement } from 'src/app/models/evenement';
import { ActivatedRoute, Router } from '@angular/router';
import { EvenementService } from 'src/app/services/evenement.service';

@Component({
  selector: 'app-detail-pro',
  templateUrl: './detail-pro.component.html',
  styleUrls: ['./detail-pro.component.scss']
})
export class DetailProComponent implements OnInit {
  evenement:Evenement;
  id:string;
  contenu:string;

  constructor(private route: ActivatedRoute, private evenementService:EvenementService, private router:Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;       
    });
    this.evenementService.getById(this.id).subscribe(
      (data:Evenement) => {this.evenement = data;},
      err => {console.log(err)}
    );
    setTimeout( () =>{
      this.contenu = this.evenement.contenu;
    },100);
  }
  supprimer = () => {
    this.evenementService.deleteById(this.id).subscribe(
      () => {},
      err => {console.log(err)}
    );
    this.router.navigate(["/affichagePro"])
  }
  modification = () => {
    this.router.navigate(["/modification/"+this.id]);
  }

}
