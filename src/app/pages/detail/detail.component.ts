import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Evenement } from 'src/app/models/evenement';
import { EvenementService } from 'src/app/services/evenement.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  evenement:Evenement;
  id:string;
  liens:string[];
  contenu:string;

  constructor(private route: ActivatedRoute, private evenementService:EvenementService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;       
    });
    this.evenementService.getById(this.id).subscribe(
      (data:Evenement) => {this.evenement = data;},
      err => {console.log(err)}
    );
    setTimeout( () =>{
      this.liens = this.evenement.professionnel.lienSociaux.slice();
      this.contenu = this.evenement.contenu;
    },100);
  }

}
