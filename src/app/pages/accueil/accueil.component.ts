import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Evenement } from 'src/app/models/evenement';
import { ConnexionService } from 'src/app/services/connexion.service';



@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  villeForm:FormGroup;
  data:Evenement[];
  
  
  
  
  constructor(private fb:FormBuilder, private router:Router,private connexionService:ConnexionService) {
    this.villeForm = fb.group({
      ville:""
    })
   }

  ngOnInit() {}

  recherche = () => {
    localStorage.setItem("lieu",this.villeForm.value.ville);
    this.router.navigate(['/affichage']);
  }

  inscriptionParticulier = () => {
    localStorage.setItem("status","particulier");
    this.router.navigate(['/inscription']);
    
  }
  inscriptionProfessionnel = () => {
    localStorage.setItem("status","professionnel");
    this.router.navigate(['/inscription']);
    
  }
  

}
