import { Component, OnInit } from '@angular/core';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  particulier:boolean;
  professionnel:boolean;

  constructor() {
   
   }

  ngOnInit() {
    if(localStorage.getItem("status")==="particulier"){
      this.particulier=true;
    }else if(localStorage.getItem("status")==="professionnel"){
      this.professionnel=true;
    }
    
  }

}
