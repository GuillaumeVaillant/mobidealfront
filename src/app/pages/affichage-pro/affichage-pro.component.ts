import { Component, OnInit } from '@angular/core';
import { Evenement } from 'src/app/models/evenement';
import { Professionnel } from 'src/app/models/professionnel';
import { ActivatedRoute } from '@angular/router';
import { EvenementService } from 'src/app/services/evenement.service';
import { ProfessionnelService } from 'src/app/services/professionnel.service';
import { Utilisateur } from 'src/app/models/utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-affichage-pro',
  templateUrl: './affichage-pro.component.html',
  styleUrls: ['./affichage-pro.component.scss']
})
export class AffichageProComponent implements OnInit {
  data:Evenement[];
  id:string;
  professionnel:Professionnel;
  displayedColumns: string[] = ['nom','dateDebut', 'dateFin'];
  utilisateur:Utilisateur;

  constructor(private route: ActivatedRoute, private evenementService:EvenementService, private professionnelService:ProfessionnelService, private utilisateurService:UtilisateurService,private popup: MatSnackBar) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;       
    });
    this.evenementService.getAllEvenementByIdProfessionnel(this.id).subscribe(
      (data:Evenement[])=>{ this.data = data;},
        err => {console.warn(err)}
    );
    this.professionnelService.getById(this.id).subscribe(
      (data:Professionnel) => {this.professionnel = data;},
      err => {console.log(err)}
    );
    }
  ajouter = () => {
    this.utilisateurService.getById(localStorage.getItem("id")).subscribe(
      (data:Utilisateur) => {this.utilisateur = data;},
      err => {console.log(err);}
    );
    setTimeout( () =>{
      this.utilisateur.professionnels.push(this.professionnel);
    
      this.utilisateurService.postUtilisateur(this.utilisateur).subscribe(
        () => {},
          err => console.warn(err)
      );
      this.popup.open("Ce mobidealer a été ajouté à vos favoris","Fermer");

    },100);

  }

}
