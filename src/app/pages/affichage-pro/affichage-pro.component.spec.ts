import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageProComponent } from './affichage-pro.component';

describe('AffichageProComponent', () => {
  let component: AffichageProComponent;
  let fixture: ComponentFixture<AffichageProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
