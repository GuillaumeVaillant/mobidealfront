import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { Utilisateur } from 'src/app/models/utilisateur';
import { Professionnel } from 'src/app/models/professionnel';
import { Router } from '@angular/router';
import { ConnexionService } from 'src/app/services/connexion.service';
import { ProfessionnelService } from 'src/app/services/professionnel.service';

@Component({
  selector: 'app-favoris',
  templateUrl: './favoris.component.html',
  styleUrls: ['./favoris.component.scss']
})
export class FavorisComponent implements OnInit {
  utilisateur:Utilisateur;
  professionnels:Professionnel[];
  vide:boolean;
  pro:Professionnel[];
  

  constructor(private utilisateurService:UtilisateurService, private router:Router,private professionnelService:ProfessionnelService) { }

  ngOnInit() {
    this.utilisateurService.getById(localStorage.getItem("id")).subscribe(
      (data:Utilisateur) => {this.utilisateur = data;},
      err => {console.log(err);}
    );
    setTimeout( () =>{
      if(this.utilisateur.professionnels.length===0) {
        this.vide=true;
      }else{
        this.vide=false;
        this.professionnels = this.utilisateur.professionnels.slice();
      }       
      
    },100);
  }
  supprimer = (i:number) => {
    this.utilisateur.professionnels.splice(i,1);
    this.utilisateurService.postUtilisateur(this.utilisateur).subscribe(
      () => {},
      err => console.warn(err)
    );
    location.reload();
  }
  


  

}
