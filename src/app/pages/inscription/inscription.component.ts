import { Component, OnInit } from '@angular/core';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {
  particulier:boolean = false;
  professionnel:boolean = false

  constructor(private connexionService:ConnexionService) { }

  ngOnInit() {
    if(localStorage.getItem("status")==="particulier"){
      this.particulier = true;
    }else if(localStorage.getItem("status")==="professionnel"){
      this.professionnel = true;
    }
    
    
  }

}
