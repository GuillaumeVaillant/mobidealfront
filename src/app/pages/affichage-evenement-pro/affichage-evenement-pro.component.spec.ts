import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageEvenementProComponent } from './affichage-evenement-pro.component';

describe('AffichageEvenementProComponent', () => {
  let component: AffichageEvenementProComponent;
  let fixture: ComponentFixture<AffichageEvenementProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageEvenementProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageEvenementProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
