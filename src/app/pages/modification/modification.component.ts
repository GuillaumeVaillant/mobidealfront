import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Professionnel } from 'src/app/models/professionnel';
import { Evenement } from 'src/app/models/evenement';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfessionnelService } from 'src/app/services/professionnel.service';
import { EvenementService } from 'src/app/services/evenement.service';

@Component({
  selector: 'app-modification',
  templateUrl: './modification.component.html',
  styleUrls: ['./modification.component.scss']
})
export class ModificationComponent implements OnInit {
  creationForm:FormGroup;
  evenement:Evenement;
  id:string;

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '3000',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
     
      ['fontSize']
    ]
};
  constructor(private route: ActivatedRoute, private fb:FormBuilder,private evenementService:EvenementService,private professionnelService:ProfessionnelService,private router:Router) {
    this.creationForm = fb.group({
      nom:"",
      tag:"",
      lieu:"",
      dateDebut:"",
      dateFin:"",
      contenu:""

    })
   }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
    this.evenementService.getById(this.id).subscribe(
      (data:Evenement) => {this.evenement = data;},
      err => {console.log(err)}
    );
  }

  modifier = () =>{    
      let evenement = new Evenement();
      evenement.id = this.evenement.id;
      evenement.nom = this.creationForm.value.nom;
      evenement.tag = this.creationForm.value.tag;
      evenement.lieu = this.creationForm.value.lieu;
      evenement.dateDebut = this.creationForm.value.dateDebut;
      evenement.dateFin = this.creationForm.value.dateFin;
      evenement.contenu = this.creationForm.value.contenu;
      evenement.professionnel.id = localStorage.getItem("id")
      this.evenementService.postEvenement(evenement).subscribe(
        () => {},
        err => {console.warn(err)}
      );
      this.router.navigate(["/detailPro/"+this.id]);
  }
  

}
