import { Professionnel } from './professionnel';

export class Utilisateur {
    id:string;
    nom:string;
    prenom:string;
    adresse:string;
    telephone:string;
    mail:string;
    password:string;
    professionnels:Array<Professionnel>=new Array<Professionnel>();
    dateInscription:Date;
    
}
