import { Professionnel } from './professionnel';

export class Evenement {
    id:string;
    nom:string;
    tag:string;
    lieu:string;
    dateDebut:Date;
    dateFin:Date;
    contenu:string;
    professionnel:Professionnel=new Professionnel();
}
