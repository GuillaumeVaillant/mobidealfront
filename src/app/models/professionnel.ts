import { Evenement } from './evenement';

export class Professionnel {
    id:string;
    nom:string;
    adresse:string;
    telephone:string;
    mail:string;
    password:string;
    lienSociaux:Array<string>;
    secteurActivite:string;
    dateInscription:Date;
}
