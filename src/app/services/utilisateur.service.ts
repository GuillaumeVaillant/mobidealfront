import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Utilisateur } from '../models/utilisateur';
import { RechercheUtilisateur } from '../models/recherche-utilisateur';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  private url:string = "http://localhost:8080/utilisateurs/"
  constructor(private http:HttpClient) { }

  getAllUtilisateur = () =>{
    return this.http.get(this.url);
  }
  postUtilisateur = (u:Utilisateur) => {
    return this.http.post(this.url,u);
  }
  connexionUtilisateur = (ru:RechercheUtilisateur) => {
    return this.http.post(this.url+"mail/",ru);
  }
  getById = (id:string) => {
    return this.http.get(this.url+"id/"+`${id}`);
  }
  updateUtilisateur = (u:Utilisateur) => {
    return this.http.put(this.url,u);
  }

  deleteById = (id:string) =>{
    return this.http.delete(this.url+"id/"+`${id}`);
  }
  
}
