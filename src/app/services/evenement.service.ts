import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Evenement } from '../models/evenement';
import { RechercheEvenement } from '../models/recherche-evenement';

@Injectable({
  providedIn: 'root'
})
export class EvenementService {

  private url:string = "http://localhost:8080/evenements/"
  constructor(private http:HttpClient) { }

  getAllEvenement = () =>{
    return this.http.get(this.url);
  }
  postEvenement = (e:Evenement) => {
    return this.http.post(this.url,e);
  }
  getAllEvenementByDate = (date:string) => {
    return this.http.get(this.url+"date/"+`${date}`);
  }
  postEvenementRecherche = (re:RechercheEvenement) =>{
    return this.http.post(this.url+"recherches/",re);
  }
  getAllEvenementByLieu = (lieu:string) => {
    return this.http.get(this.url+"lieu/"+`${lieu}`);
  }
  getAllEvenementByProfessionnel = (nomProfessionnel:string) => {
    return this.http.get(this.url+"nomProfessionnel/"+`${nomProfessionnel}`);
  }
  getAllEvenementByIdProfessionnel = (idProfessionnel:string) => {
    return this.http.get(this.url+"idProfessionnel/"+`${idProfessionnel}`);
  }
  getById = (id:string) => {
    return this.http.get(this.url+"id/"+`${id}`);
  }
  getByNom = (nom:string) => {
    return this.http.get(this.url+"nom/"+`${nom}`);
  }
  deleteById = (id:String) => {
    return this.http.delete(this.url+"id/"+`${id}`)
  }

}
