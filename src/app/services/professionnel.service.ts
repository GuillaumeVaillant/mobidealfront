import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Professionnel } from '../models/professionnel';
import { RechercheProfessionnel } from '../models/recherche-professionnel';

@Injectable({
  providedIn: 'root'
})
export class ProfessionnelService {

  private url:string = "http://localhost:8080/professionnels/"
  constructor(private http:HttpClient) { }

  getAllProfessionnel = () =>{
    return this.http.get(this.url);
  }
  postProfessionnel = (p:Professionnel) => {
    return this.http.post(this.url,p);
  }
  getAllProfessionnelByNom = (nom:string) =>{
    return this.http.get(this.url+"nom/"+`${nom}`)
  }
  connexionProfessionnel = (rp:RechercheProfessionnel) =>{
    return this.http.post(this.url+"mail/",rp);
  }
  getById = (id:string) =>{
    return this.http.get(this.url+"id/"+`${id}`);
  }
  deleteById = (id:string) => {
    return this.http.delete(this.url+"id/"+`${id}`)
  }
  
}
